public class Board{
	private Tile[][] grid;
	
	public Board(){
		//size
		int boardSize = 3;
		
		grid = new Tile[boardSize][boardSize];
		
		//has to use c style for loops because 
		//for each loops cannot modify, only read
		for(int i = 0; i < grid.length; i++){
			//sets every tile i to blank constant from enum
			//extract index i of grid, so the first level
			for(int j = 0; j < grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
			} 
		}
		
	}
	
	public String toString(){
		//extra spaces in returnString for aligning column indices
		String returnString = "  ";

		//row indices, required since i am using a for each loop
		int rowIndex = 0;

		//for column index
		for(int i = 0; i < grid[0].length; i++){
			returnString += i + " ";
		}

		//line return for column indices
		returnString += "\n";

		for(Tile[] ta : grid){
			//adds index for row
			returnString += rowIndex + " ";

			for(Tile t : ta){
				//adds the value of stored tile to the output
				//incrementally builds output
				returnString += t.getName() + " ";
				
			}
			
			//new line
			//declared outside so it only adds new every
			//3 values
			returnString += "\n";
			rowIndex++;
		}
		
		return returnString;
	}
	
	public boolean placeToken(int row, int col, Tile playerToken){
		//checks you are actually placing the token inside the board
		if( (row >= grid.length || row < 0) || (col >= grid[0].length || col < 0) )
			return false;
		
		//make sure area is empty
		//grid is the board we initialized earlier in the constructor
		if(grid[row][col] != Tile.BLANK)
			return false;
		
		//if it passes all of those checks
		//update the tile
		//and return true
		else{
			grid[row][col] = playerToken;
			return true;
		}
			
	}
	
	public boolean checkIfFull(){
		//re-uses code from toString
		
		for(Tile[] ta : grid){
			//if a single t is still blank, return false
			for(Tile t : ta){
				if(t == Tile.BLANK)
					return false;
			}
			 
		}
		
		//otherwise, just return true
		return true;
	}
	
	public boolean checkIfWinning(Tile playerToken){
		if( checkIfWinningHorizontal(playerToken) || checkiIWinningfVertical(playerToken) )
			return true;
		else
			return false; 
	}


	//helpers
	
	private boolean checkIfWinningHorizontal(Tile playerToken){
		//re-uses code from toString
		
		
		
		for(Tile[] ta : grid){
			//flip flop is declared inside the for loop 
			boolean flipflop = true;

			for(Tile t : ta){
				if(t != playerToken)
					//if one is not the same as playerToken, 
					//automatically set flipflop to false
					flipflop = false;
			}
			//immediately returns flipflop after each row
			//otherwise, a single different playerToken will set flipflop to false 
			//which means that flipflop will only be true if the entire grid is filled with the same playerToken
			if(flipflop == true)
				return true;

		}
		
		//returns false normally
		return false;
	}
	
	
	private boolean checkiIWinningfVertical(Tile playerToken){
		
		
		for(int i = 0; i < grid.length; i++){
			//flip flop
			boolean flipflop = true;

			//i = num of rows
			//j = nums of columns
			for(int j = 0; j < grid[i].length; j++){
				//to check elements in column, we need to check the first element of each row
				//to do so, invert the order of the index, so the row goes up first in the inner loop
				//so it's row of column instead of column of row
				if(grid[j][i] != playerToken)
					flipflop = false;
			}
			if(flipflop == true)
				return flipflop;

		}
		
		return false;
	}

	
}