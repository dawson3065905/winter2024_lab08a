enum Tile{
	BLANK("_"),
	X("X"),
	O("O");
	
	//fields
	private String name;
	
	private Tile(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}