import java.util.Scanner;

public class TicTacToeApp {
    
    public static void main(String[] args){
        System.out.println("Welcome to Tic-Tac-Toe \n Player 1 = X \n Player 2 = O \n");

        Board board = new Board();
        boolean gameOver = false;
        //player 1's token is X
        int player = 1;
        Tile playerToken = Tile.X;

        Scanner scanner = new Scanner(System.in);

        while(gameOver != true){
            System.out.println(board.toString());

         

            if(player == 1){
                playerToken = Tile.X;
                System.out.println("Player 1's turn: \n");
            }
            if(player == 2){
                playerToken = Tile.O;
                System.out.println("Player 2's turn: \n");
            }

            System.out.println("Input the row");
            int row = scanner.nextInt();

            System.out.println("Input the column");
            int column = scanner.nextInt();

            //placing token, checks if you are not placing outside the board
            //note, placeToken both returns a boolean and modifies array
            while(board.placeToken(row, column, playerToken) != true){
                System.out.print("Invalid input, try again \n");
                
                System.out.println("Input the row (horizontal)");
                row = scanner.nextInt();
                
                System.out.println("Input the column (vertical)");
                column = scanner.nextInt();

                if(player == 1){
                    playerToken = Tile.X;
                }
                else{
                    playerToken = Tile.O;
                }
            }

            //check for winner
            if(board.checkIfWinning(playerToken)){
                //prints winner of the current turn
                System.out.println("The winner is Player " + player + "! \n");
                
                gameOver = true;
            }
            if(board.checkIfFull()){
                System.out.println("It's a tie! \n");
                gameOver = true;
            }
            else{
                //turn flip flop
                if(player == 2)
                    player = 1;
                else
                    player++;
            }

        }

        scanner.close();
    }
}
